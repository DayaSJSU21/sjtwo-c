#include "gpio_lab.h"

#include "LPC40xx.h"

void gpio0__set_as_input(uint8_t pin_num) { LPC_GPIO1->DIR &= ~(1 << pin_num); }

void gpio0__set_as_output(uint8_t pin_num) { LPC_GPIO1->DIR |= (1 << pin_num); }

void gpio0__set_high(uint8_t pin_num) { LPC_GPIO1->SET = (1 << pin_num); }

void gpio0__set_low(uint8_t pin_num) { LPC_GPIO1->CLR = (1 << pin_num); }

void gpio0__set(uint8_t pin_num, bool high) {
  if (high == true) {
    LPC_GPIO1->SET = (1 << pin_num);
  } else {
    LPC_GPIO1->CLR = (1 << pin_num);
  }
}

bool gpio0__get_level(uint8_t pin_num) {
  bool result;
  if (LPC_GPIO1->PIN & (1 << pin_num)) {
    result = true;
  } else {
    result = false;
  }
  return result;
}
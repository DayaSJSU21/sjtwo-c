#include "FreeRTOS.h"
#include "LPC40xx.h"
#include "board_io.h"
#include "common_macros.h"
#include "gpio_lab.h"
#include "periodic_scheduler.h"
#include "semphr.h"
#include "sj2_cli.h"
#include "stdint.h"
#include "task.h"
#include <stdio.h>
#define task_1
#define task_2
#define task_3

///////////////////////task3///////////////////////////////////////////////
#ifdef task_3
static SemaphoreHandle_t switch_press_indication;
#endif
/////////////////////task3/////////////////////////////////////////////////

///////////////////////task1///////////////////////////////////////////////
/*#ifdef task_1
void led_task(void *pvParameters);
#endif*/
/////////////////////task1/////////////////////////////////////////////////

// 'static' to make these functions 'private' to this file
// static void create_blinky_tasks(void);
static void create_uart_task(void);
// static void blink_task(void *params);
static void uart_task(void *params);

typedef struct {
  /* First get gpio0 driver to work only, and if you finish it
   * you can do the extra credit to also make it work for other Ports
   */
  // uint8_t port;
  uint8_t pin;
} port_pin_s;

///////////////////////////////////Task2///////////////////////////////
/*
#ifdef task_2
void led_task(void *task_parameter) {
  // Type-cast the paramter that was passed from xTaskCreate()
  const port_pin_s *led = (port_pin_s *)(task_parameter);

  while (true) {
    gpio0__set_high(led->pin);
    vTaskDelay(2000);

    gpio0__set_low(led->pin);
    vTaskDelay(2000);
  }
}
#endif
*/
////////////////////////////////Task2/////////////////////////////////

//////////////////////////task3/////////////////////////////////////////
#ifdef task_3
void led_task(void *task_parameter) {
  const port_pin_s *led = (port_pin_s *)(task_parameter);
  gpio0__set_as_output(led->pin);
  while (true) {
    // Note: There is no vTaskDelay() here, but we use sleep mechanism while waiting for the binary semaphore (signal)
    if (xSemaphoreTake(switch_press_indication, 1000)) {
      gpio0__set_high(led->pin);
      vTaskDelay(1000);

      gpio0__set_low(led->pin);
      vTaskDelay(1000);
    } else {
      puts("Timeout: No switch press indication for 1000ms");
    }
  }
}

void switch_task(void *task_parameter) {
  port_pin_s *switch1 = (port_pin_s *)task_parameter;

  while (true) {
    // TODO: If switch pressed, set the binary semaphore
    if (gpio0__get_level(switch1->pin)) {
      xSemaphoreGive(switch_press_indication);
    }
    // Task should always sleep otherwise they will use 100% CPU
    // This task sleep also helps avoid spurious semaphore give during switch debeounce
    vTaskDelay(100);
  }
}
#endif
//////////////////////////task3////////////////////////////////////////////

int main(void) {
  // create_blinky_tasks();
  create_uart_task();
// If you have the ESP32 wifi module soldered on the board, you can try uncommenting this code
// See esp32/README.md for more details
// uart3_init();                                                                     // Also include:  uart3_init.h
// xTaskCreate(esp32_tcp_hello_world_task, "uart3", 1000, NULL, PRIORITY_LOW, NULL); // Include esp32_task.h
// printf("CMPE 244 Hello");

// TODO: Instantiate a struct of type my_s with the name of "s"

/////////////////////////////Task3/////////////////////////////////////////
#ifdef task_3
  switch_press_indication = xSemaphoreCreateBinary();

  static port_pin_s led0 = {26};
  static port_pin_s switch1 = {15};

  xTaskCreate(led_task, "Led0", 1024, &led0, PRIORITY_LOW, NULL);
  xTaskCreate(switch_task, "Switch1", 1024, &switch1, PRIORITY_LOW, NULL);
#endif
  ////////////////////////////Task3/////////////////////////////////////////

  ///////////////////////////////Task2/////////////////////////////////////
  /*
  #ifdef task_2
    static port_pin_s led0 = {24};
    static port_pin_s led1 = {26};

    xTaskCreate(led_task, "led0", 2048 / sizeof(void *), &led0, PRIORITY_LOW,
    xTaskCreate(led_task, "led1", 2048 / sizeof(void *), &led1, PRIORITY_LOW, NULL);
  #endif*/
  ////////////////////////////////////////Task2///////////////////////////////

  /////////////////////////////Task1/////////////////////////////////////////
  /*#ifdef task_1
  xTaskCreate(led_task, "led2", 2048 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  #endif*/
  /////////////////////////////Task1/////////////////////////////////////////

  puts("Starting RTOS");
  vTaskStartScheduler(); // This function never returns unless RTOS scheduler runs out of memory and fails

  return 0;
}

/////////////////////////////Task1/////////////////////////////////////////
/*#ifdef task_1
void led_task(void *pvParameters) {
  // Choose one of the onboard LEDS by looking into schematics and write code for the below
  // 0) Set the IOCON MUX function(if required) select pins to 000
  // 1) Set the DIR register bit for the LED port pin
  const uint32_t led2 = (1 << 24);
  LPC_GPIO1->DIR |= led2;

  while (1) {
    // 2) Set PIN register bit to 0 to turn ON LED (led may be active low)
    LPC_GPIO1->PIN |= led2;
    vTaskDelay(2000);

    // 3) Set PIN register bit to 1 to turn OFF LED
    LPC_GPIO1->PIN &= ~led2;
    vTaskDelay(2000);
  }
}
#endif*/
/////////////////////////////Task1/////////////////////////////////////////

/*
static void create_blinky_tasks(void) {
  /**
   * Use '#if (1)' if you wish to observe how two tasks can blink LEDs
   * Use '#if (0)' if you wish to use the 'periodic_scheduler.h' that will spawn 4 periodic tasks, one for each LED
   */
/*#if (1)
  // These variables should not go out of scope because the 'blink_task' will reference this memory
  static gpio_s led0, led1;

  // If you wish to avoid malloc, use xTaskCreateStatic() in place of xTaskCreate()
  static StackType_t led0_task_stack[512 / sizeof(StackType_t)];
  static StackType_t led1_task_stack[512 / sizeof(StackType_t)];
  static StaticTask_t led0_task_struct;
  static StaticTask_t led1_task_struct;

  led0 = board_io__get_led0();
  led1 = board_io__get_led1();

  xTaskCreateStatic(blink_task, "led0", ARRAY_SIZE(led0_task_stack), (void *)&led0, PRIORITY_LOW, led0_task_stack,
                    &led0_task_struct);
  xTaskCreateStatic(blink_task, "led1", ARRAY_SIZE(led1_task_stack), (void *)&led1, PRIORITY_LOW, led1_task_stack,
                    &led1_task_struct);
#else
  periodic_scheduler__initialize();
  UNUSED(blink_task);
#endif
}*/

static void create_uart_task(void) {
  // It is advised to either run the uart_task, or the SJ2 command-line (CLI), but not both
  // Change '#if (0)' to '#if (1)' and vice versa to try it out
#if (0)
  // printf() takes more stack space, size this tasks' stack higher
  xTaskCreate(uart_task, "uart", (512U * 8) / sizeof(void *), NULL, PRIORITY_LOW, NULL);
#else
  sj2_cli__init();
  UNUSED(uart_task); // uart_task is un-used in if we are doing cli init()
#endif
}

/*static void blink_task(void *params) {
  const gpio_s led = *((gpio_s *)params); // Parameter was input while calling xTaskCreate()

  // Warning: This task starts with very minimal stack, so do not use printf() API here to avoid stack overflow
  while (true) {
    gpio__toggle(led);
    vTaskDelay(500);
  }
}*/

// This sends periodic messages over printf() which uses system_calls.c to send them to UART0
static void uart_task(void *params) {
  TickType_t previous_tick = 0;
  TickType_t ticks = 0;

  while (true) {
    // This loop will repeat at precise task delay, even if the logic below takes variable amount of ticks
    vTaskDelayUntil(&previous_tick, 2000);

    /* Calls to fprintf(stderr, ...) uses polled UART driver, so this entire output will be fully
     * sent out before this function returns. See system_calls.c for actual implementation.
     *
     * Use this style print for:
     *  - Interrupts because you cannot use printf() inside an ISR
     *    This is because regular printf() leads down to xQueueSend() that might block
     *    but you cannot block inside an ISR hence the system might crash
     *  - During debugging in case system crashes before all output of printf() is sent
     */
    ticks = xTaskGetTickCount();
    fprintf(stderr, "%u: This is a polled version of printf used for debugging ... finished in", (unsigned)ticks);
    fprintf(stderr, " %lu ticks\n", (xTaskGetTickCount() - ticks));

    /* This deposits data to an outgoing queue and doesn't block the CPU
     * Data will be sent later, but this function would return earlier
     */
    ticks = xTaskGetTickCount();
    printf("This is a more efficient printf ... finished in");
    printf(" %lu ticks\n\n", (xTaskGetTickCount() - ticks));
  }
}
